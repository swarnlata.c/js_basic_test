var assert = require('assert');
const test = [
 {
   "p": 73,
   "r": 98,
   "t": 11
 },
 {
   "p": 25,
   "r": 37,
   "t": 77
 },
 {
   "p": 34,
   "r": -44,
   "t": 69
 },
 {
   "p": 86,
   "r": 25,
   "t": 27
 },
 {
   "p": 27,
   "r": -38,
   "t": 11
 },
 {
   "p": 4,
   "r": 11,
   "t": 66
 }
]
function interest(arr)
{
  let ans= [];

  for(let i=0;i<arr.length;i++)
  {
    ans[i]= (arr[i].p * arr[i].r * arr[i].t)/100;
  }
  for(let i=0;i<ans.length;i++)
  {
    if(ans[i]<0)
    {
      ans[i]=0;
    }
  }
  return ans;

}

console.log(interest(test));

//test case 1
const test1 = [
  {
    "p": 73,
    "r": -98,
    "t": 11
  },
  {
    "p": 25,
    "r": 37,
    "t": 77
  },
  
 ]

 //console.log(interest(test1));
 assert.deepEqual(interest(test1), [0,712.25]);

 //test case 2
 const test2 = [
 
  {
    "p": -86,
    "r": 25,
    "t": 27
  },
  {
    "p": 27,
    "r": -38,
    "t": 11
  },
  {
    "p": 4,
    "r": 11,
    "t": 66
  }
 ]
 //console.log(interest(test2));
 assert.deepEqual(interest(test2), [ 0, 0, 29.04 ])

//test case 3
const test3 = [
  {
    "p": 6,
    "r": 5,
    "t": 27
  },
  {
    "p": 7,
    "r": 8,
    "t": 11
  }
]

//console.log(interest(test3));
assert.deepEqual(interest(test3), [8.1, 6.16]);
	

