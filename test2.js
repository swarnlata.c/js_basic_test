var arr1 = ['0e089d7ee981bec654acb4a678cef7e6', '7adefed65ed36cb257a5e44bc0ad9919', '64450e7b290f8abcfb070a27d5eaf202'];
var arr2 = [{
   "hashKey" : "7adefed65ed36cb257a5e44bc0ad9919",
   "sizeInBytes" : 127543,
   "storePath" : "arxiv/pdf/astro-ph0001011.pdf",
}, {
   "hashKey" : "0e089d7ee981bec654acb4a678cef7e6",
   "sizeInBytes" : 197563,
   "storePath" : "arxiv/pdf/astro-ph0001018.pdf",
}, {}, {
   "sizeInBytes" : 15705,
   "storePath" : "arxiv/pdf/astro-ph0001010.pdf",
}];
// There is an array (arr1) containing hash value and a second array (arr2) contains array of objects. 
// Sort arr2 in the same order as arr1 using "hashKey". Come up with an optimized solution.

var ans = [];

let k=0;
let t=0;
for(let i=0;i<arr1.length;i++)
{
	for(let j=0;j<arr2.length;j++)
	{
		if(arr1[i] == arr2[j].hashKey)
		{
			ans[k]=arr2[j];
			k++;
			arr2[j].hashKey=0;
		}
		
	}
}
for(let j=0;j<arr2.length;j++)
{
		if(arr2[j].hashKey !==0)
		{
			ans[k]=arr2[j];
			k++;
		}
}
console.log(ans);

