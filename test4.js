var assert = require('assert');
var voters = [
   {name:'Bob' , age: 30, voted: true},
   {name:'Jake' , age: 32, voted: true},
   {name:'Kate' , age: 25, voted: false},
   {name:'Sam' , age: 20, voted: false},
   {name:'Phil' , age: 21, voted: true},
   {name:'Ed' , age:55, voted:true},
   {name:'Tami' , age: 54, voted:true},
   {name: 'Mary', age: 31, voted: false},
   {name: 'Becky', age: 43, voted: false},
   {name: 'Joey', age: 41, voted: true},
   {name: 'Jeff', age: 30, voted: true},
   {name: 'Zack', age: 19, voted: false}
];
function voterResults(arr) {
 
  const ans = { 
    youngVotes: 0,
    youth: 0,
    midVotes: 0,
    mids: 0,
    oldVotes: 0,
    olds: 0

  }

  for(let i=0;i<arr.length;i++)
  {
    if(arr[i].age>=18 && arr[i].age<=25)
    {
      ans.youth++;
      if(arr[i].voted===true)
      {
        ans.youngVotes++;
      }
    }
    if(arr[i].age>=26 && arr[i].age<=35)
    {
      ans.mids++;
      if(arr[i].voted===true)
      {
        ans.midVotes++;
      }
    }
    if(arr[i].age>=36 && arr[i].age<=55)
    {
      ans.olds++;
      if(arr[i].voted===true)
      {
        ans.oldVotes++;
      }
    }

  }
 
  return ans;
  // your code here
}
//console.log(voterResults(voters)); 

//test case 
var voters1 = [
  {name:'Bob' , age: 30, voted: true},
  {name:'Jake' , age: 32, voted: true},
  {name:'Kate' , age: 25, voted: false},
  {name:'Sam' , age: 20, voted: false},
  {name:'Phil' , age: 21, voted: true},
  {name:'Ed' , age:55, voted:true},
];

assert.deepEqual(voterResults(voters1), { youngVotes: 1, youth: 3, midVotes: 2, mids: 2, oldVotes: 1, olds: 1 }
)
console.log(voterResults(voters1));

//test case
var voters2 = [
  {name:'Jake' , age: 32, voted: true},
  {name:'Kate' , age: 25, voted: false},
  {name:'Phil' , age: 21, voted: true},
  {name:'Ed' , age:55, voted:true},
];

assert.deepEqual(voterResults(voters2), { youngVotes: 1, youth: 2, midVotes: 1, mids: 1, oldVotes: 1, olds: 1 }
)
//console.log(voterResults(voters2));


